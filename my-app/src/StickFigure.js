import './StickFigure.css';
import React from 'react';

function StickFigure(props) {
    return (
        <div className="StickFigure-container">
            <div>*INSERT POORLY DRAWN STICK FIGURE HERE*</div>
            <div>Lives remaining: {props.lives}</div>
        </div>
    )
}


export default StickFigure;