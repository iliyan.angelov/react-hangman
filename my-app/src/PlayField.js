import './PlayField.css';
import LettersContainer from './LettersContainer';
import WordToGuess from './WordToGuess';
import StickFigure from './StickFigure';
import React, { useState, useEffect } from 'react';


function PlayField(props) {
    const [wordToGuess, updateWordToGuess] = useState('loading');
    const [chosenLetter, updateChosenLetter] = useState('');
    const [myLives, updateRemainingLives] = useState(5);

    useEffect(() => {
        let id = Math.floor(Math.random() * 80) + 1;
        fetch(`https://swapi.dev/api/people/${id}/`)
            .then(res => res.json())
            .then((result) => {
                //throws undefinied for the name from time to time. Probably due to a lot of requests
                updateWordToGuess(result.name.toUpperCase());
                console.log('passing through fetch phase');
            });
    }, [])

    function onLetterClick(chosenLetter) {
        updateChosenLetter(chosenLetter);
    }

    function onInCorrectLetterGuess(chosenLetter) {
        if (chosenLetter) {
            updateRemainingLives(myLives - 1);
        }
    }

    return (
        <div className="PlayField-container">
            <h3 className="PlayField-header">Welcome traveller</h3>
            <LettersContainer onLetterClick={onLetterClick} />
            <WordToGuess wordToGuess={wordToGuess} chosenLetter={chosenLetter} onIncorrectGuess={onInCorrectLetterGuess} />
            <StickFigure lives = {myLives}/>
        </div>
    );
}

export default PlayField;