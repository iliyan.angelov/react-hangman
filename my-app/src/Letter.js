import './Letter.css'
import React from 'react';

function Letter(props) {

    function letterClicked(e){
        props.onLetterClick(e.target.innerText);
    }

    let style; 
    console.log(props.toBeHidden);
    if (props.toBeHidden) {
        style = 'Letter-hidden-btn';
    }

    return (
        <button className={style} onClick={props.disableClick ? null: letterClicked}>{props.letter}</button>
    );
}

export default Letter;