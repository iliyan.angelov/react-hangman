import Letter from './Letter';
import './LettersContainer.css';

function LettersContainer(props) {
    return (
        <div className="LettersContainer-body">
            <h4>Choose wisely</h4>
            <div className="LettersContainer-letters">
                {createLetters(props)}
            </div>
        </div>
    );
}

function createLetters(props) {
    let letters = [];
    for (let index = 65; index <= 90; index++) {
        letters.push(<Letter letter={String.fromCharCode(index)} key={index} onLetterClick={props.onLetterClick} />);
    }
    return letters;
}

export default LettersContainer;