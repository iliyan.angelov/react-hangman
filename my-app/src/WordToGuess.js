import './WordToGuess.css'
import Letter from './Letter';
import React, { useState, useEffect } from 'react';

function WordToGuess(props) {
    let wordToGuess = props.wordToGuess;
    let chosenLetter = props.chosenLetter;
    let  wordAsContainer = <Letter letter="loading"></Letter>;
    const [allChosenLetters, updateChosenLetters] = useState([]);
    const [elementToGuess, updateElementToGuess] = useState(wordAsContainer);


    useEffect(() => {
        if (!allChosenLetters.includes(chosenLetter) && chosenLetter) {
            allChosenLetters.push(chosenLetter);
            if (wordToGuess && !wordToGuess.split('').includes(chosenLetter)) {
                props.onIncorrectGuess(chosenLetter);
            }
        }
    })

    useEffect(() => {
        console.log(chosenLetter + ' in the use effect');
        if (wordToGuess) {
            console.log(wordToGuess + ' in the hiding all letters');
            wordAsContainer = wordToGuess.split('').map(x => hideAllLetters(x, allChosenLetters));
        }
        console.log(wordAsContainer);
        updateElementToGuess(wordAsContainer);
    }, [wordToGuess, chosenLetter]);

    return (
        <div className="wordContainer">
            <label>Guess the word</label>
            <div>
                {elementToGuess}
            </div>
        </div>
    );
}

function hideAllLetters(character, allChosenLetters) {
    let charCode = character.charCodeAt(0);
    let hide = false;
    if (charCode >= 65 && charCode <= 90 && !allChosenLetters.includes(character)) {
        hide = true;
    }
    return <Letter letter={character} toBeHidden={hide} disableClick = {true}></Letter>;
}

export default WordToGuess;