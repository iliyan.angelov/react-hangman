import logo from './logo.svg';
import './App.css';
import React from 'react';
import PlayField from './PlayField';

function App() {
  return (
    <PlayField />
  );
}

export default App;
